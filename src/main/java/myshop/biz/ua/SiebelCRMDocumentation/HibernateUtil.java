/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author vashurinvlad
 */
public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private HibernateUtil() {}
    static {
        try {
            sessionFactory= new Configuration().configure().buildSessionFactory();
        } catch (Throwable e) { throw new ExceptionInInitializerError();
        }
    }
    public static SessionFactory getSessionFactory () {
        return sessionFactory;
    }
}
