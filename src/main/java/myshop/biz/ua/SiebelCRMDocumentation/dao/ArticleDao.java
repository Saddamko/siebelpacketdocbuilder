/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation.dao;

import java.sql.SQLException;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.Article;
import java.util.List;
import myshop.biz.ua.SiebelCRMDocumentation.HibernateUtil;

/**
 *
 * @author vashurinvlad
 */
public interface ArticleDao {
    public void addArticle (Article article) throws SQLException;
    public void deleteArticle (Article article) throws SQLException;
    public Article getArticle (int id) throws SQLException;
    public List<Article> getArticles() throws SQLException;
    public Article findById(int id)  throws SQLException;
    public Article findByTitle(String Title) throws SQLException;
    
}
