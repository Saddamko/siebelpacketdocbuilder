//https://github.com/ullenboom/jrtf
package myshop.biz.ua.SiebelCRMDocumentation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.tutego.jrtf.*;
import static com.tutego.jrtf.Rtf.*;
import static com.tutego.jrtf.RtfDocfmt.defaultTab;
import static com.tutego.jrtf.RtfDocfmt.revisionMarking;
import static com.tutego.jrtf.RtfHeader.font;
import static com.tutego.jrtf.RtfPara.*;
import static com.tutego.jrtf.RtfText.*;
import static com.tutego.jrtf.RtfUnit.CM;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
import java.io.*;
import com.tutego.jrtf.RtfHeaderFont;
import static com.tutego.jrtf.RtfInfo.author;
import static com.tutego.jrtf.RtfInfo.title;
import com.tutego.jrtf.RtfPicture.PictureType;
import static com.tutego.jrtf.RtfPicture.PictureType.AUTOMATIC;
import java.awt.Desktop;
import java.util.ArrayList;
import java.util.List;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.Article;
import myshop.biz.ua.SiebelCRMDocumentation.service.UserService;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.User;
import myshop.biz.ua.SiebelCRMDocumentation.dao.ArticleDao;



@SpringBootApplication
public class Application {
    static boolean bMessageCategory, bScreen, bTask, bApplet, bBusinessComponent, bPickList;
    static boolean bBusinessObject, bIntegrationObject, bLink,  bSymbolicString,  bTable, bView, bWorkflowProcess; 

	public static void main(String[] args) throws IOException, Exception   {
	//SpringApplication.run(Application.class, args);
        
        Factory factory = Factory.getInstance();
        ArticleDao articleDao;
        articleDao = Factory.getArticleDao();
        
        /*Article article =new Article();
        article.setTitle("Title1");
        article.setArticle("Article1");
        articleDao.addArticle(article);
       
        
        List<Article> articles =articleDao.getArticles();
        
        for (Article article1: articles){
            System.out.println("Title:" +article1.getTitle());
        }
         */
        //articleDao.addArticle(article);
        
        //UserService userService = new UserService();
        //User user = new User("Masha");
        //userService.saveUser(user);
        int Process = 0;
        int Thread = 0;
        
        String FileName = "C:\\sources\\git\\Java\\SiebelPacketDocBuilder\\Resources\\PkgUnq032.txt";

        if (args.length == 1)   {
                                    FileName = (args[0]);
                                }
    
          /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameForm().setVisible(true);
            }
        });
        
        ScanForKind (FileName) ;
        BuildDocFile (FileName) ;
}
     
        
static void ScanForKind(String FileName) throws FileNotFoundException, IOException {
    String strType="Business Component";
    if (IsTypeExists(FileName, strType)==true)
            {
                bBusinessComponent=true;
                System.out.println(strType);
            }
    strType="Applet";
    if (IsTypeExists(FileName, strType)==true)
            {
                bApplet=true;
                System.out.println(strType);
            }
    strType="Pick List";
    if (IsTypeExists(FileName, strType)==true)
            {
                bPickList=true;
                System.out.println(strType);
            }
    strType="Business Object";
    if (IsTypeExists(FileName, strType)==true)
            {
                bBusinessObject=true;
                System.out.println(strType);
            }    
    strType="Integration Object";
    if (IsTypeExists(FileName, strType)==true)
            {
                bIntegrationObject=true;
                System.out.println(strType);
            }
    strType="Link";
    if (IsTypeExists(FileName, strType)==true)
            {
                bLink=true;
                System.out.println(strType);
            }
    strType="Message Category";
    if (IsTypeExists(FileName, strType)==true)
            {
                bMessageCategory=true;
                System.out.println(strType);
            }  
    strType="Symbolic String";
    if (IsTypeExists(FileName, strType)==true)
            {
                bSymbolicString=true;
                System.out.println(strType);
            }
    strType="Table";
    if (IsTypeExists(FileName, strType)==true)
            {
                bTable=true;
                System.out.println(strType);
            }    
    strType="View";
    if (IsTypeExists(FileName, strType)==true)
            {
                bView=true;
                System.out.println(strType);
            }
    strType="Workflow Process";
    if (IsTypeExists(FileName, strType)==true)
            {
                bWorkflowProcess=true;
                System.out.println(strType);
            }     
    strType="Task";
    if (IsTypeExists(FileName, strType)==true)
            {
                bTask=true;
                System.out.println(strType);
            }  
    strType="Screen";
    if (IsTypeExists(FileName, strType)==true)
            {
                bScreen=true;
                System.out.println(strType);
            }     
}        

static boolean IsTypeExists(String FileName, String strType) throws FileNotFoundException, IOException {
    try(BufferedReader br = new BufferedReader(new FileReader(FileName))) {
    StringBuilder sb = new StringBuilder();
    String line = br.readLine();
        while (line != null) {
            if (line.contains(strType) ) {
                return true;
            }
            line = br.readLine();
        }
        return false;
    }
}
                
static void BuildDocFile (String FileName) throws FileNotFoundException, IOException, Exception {
String str="", str1="", str2="";
String strObj="";
Integer index1, index2;
File out = new File( "C:\\sources\\git\\Java\\SiebelPacketDocBuilder\\Resources\\out.rtf" );
FileWriter i = new FileWriter( out  );
System.out.println("Starting Documentation Builder...");

        Factory factory = Factory.getInstance();
        ArticleDao articleDao;
        articleDao = Factory.getArticleDao();
                
 try(BufferedReader br = new BufferedReader(new FileReader(FileName))) {
    StringBuilder sb = new StringBuilder();
    String line = br.readLine();
    String Arr[];
    Rtf myRtf;
    RtfPara ParCommon,ParBC,  ParTest, ParApplyTable = null;
    RtfText t;
    RtfInfo info1 = null, info2 = null;
    RtfHeader Header = null;
    String strBC="",  strTable="", strWorkflow="";
    ArrayList<String> saTable = new ArrayList<String>();
    ArrayList<String> saWorkflow = new ArrayList<String>();
    ArrayList<String> saBC = new ArrayList<String>();

    ParCommon =RtfPara.p();

    System.out.println("Business component..");
    while (line != null) {
              
        sb.append(line);
        sb.append(System.lineSeparator());
        Arr = line.split(",");
        strObj= "Business Component";
        if (line.contains(strObj) ) {
            str= line.substring(line.indexOf(strObj), line.indexOf(",C:\\Packet"));
            System.out.print(Arr[0]);
            System.out.print("  ");
            System.out.print(Arr[1]);
            System.out.print(" is located at ");
            System.out.println(Arr[2]);
            strBC= strBC +"\n " + Arr[1];
        }
        strObj= "Table";
        if (line.contains(strObj) ) {
           str= line.substring(line.indexOf(strObj), line.indexOf(",C:\\Packet")); 
           strTable= strTable +"\n " + Arr[1];
              
        }
        
        strObj= "Workflow Process";
        if (line.contains(strObj) ) {
           str= line.substring(line.indexOf(strObj), line.indexOf(",C:\\Packet")); 
           strWorkflow= strWorkflow +"\n " + Arr[1];
           saWorkflow.add(Arr[1]);
              
        }
        line = br.readLine();
        
    }
    ParBC = RtfPara.p( strBC);
    String everything = sb.toString();
    
    //Doc header
    myRtf = Rtf.rtf()
        .info( author("Владимир Вашурин"), title("Информация о пакете") )
        .documentFormatting( defaultTab( 1, CM ),revisionMarking() )
        .header(font( "Calibri" ).at( 0 ) )                   
        ;
    //Intro
    myRtf.section(p(bold(text("Инструкция к пакету"))).alignCentered().indentFirstLine( 0.25, RtfUnit.CM ));
    
    
    //Common section
        Article Common= articleDao.findByTitle("Common");
        String sCommon=Common.getArticle();   
        if (sCommon.isEmpty())
        {
        } else 
        {myRtf.section(p(sCommon));}
        
        myRtf.section(p(bold(text("Описание пакета"))).alignCentered().indentFirstLine( 0.25, RtfUnit.CM ),
        p(italic(text("Состав объектов пакета"))).alignCentered().indentFirstLine( 0.25, RtfUnit.CM ),
                ParBC);
        
        //Package files location
        //Repository Object import
        Article Location= articleDao.findByTitle("Location");
        String sLocation=Location.getArticle();
        
        if (sLocation.isEmpty())
        {
        } else {
            myRtf
                    .section(p(sLocation));
        }
        
        //Repository Object import
        Article RepositoryObjectsImport= articleDao.findByTitle("RepositoryObjectsImport");
        String sRepositoryObjectsImport=RepositoryObjectsImport.getArticle();
        
        if (sRepositoryObjectsImport.isEmpty())
        {
        } else {
            myRtf
                    .section(p(sRepositoryObjectsImport));
        }

        //Apply table
        if (bTable)
        {  
            Article TableApply= articleDao.findByTitle("TableApply");
            String sTableApply=TableApply.getArticle();

            if (sTableApply.isEmpty())
            {
            } else {
                myRtf
                        .section(p(sTableApply),
                        p( picture( getResource( "ToolsLanguage.png" ) ).size(15, 10, RtfUnit.CM).type( AUTOMATIC )),
                        p(bold(text("Перечень таблиц:"))).alignCentered().indentFirstLine( 0.25, RtfUnit.CM ),
                        p(text(strTable)),
                        p( picture( getResource( "ApplySchema.png" ) ).size(7, 10, RtfUnit.CM).type( AUTOMATIC ))
                        );
            }
        }
        
        if (bWorkflowProcess)
        {
            Article WorkflowProcess= articleDao.findByTitle("WorkflowProcess");
            String sWorkflowProcess=WorkflowProcess.getArticle();

            if (sWorkflowProcess.isEmpty())
            {
            } else {
                myRtf
                        .section(p(sWorkflowProcess),
                        p( picture( getResource( "WorkflowProcess1.png" ) ).size(15, 10, RtfUnit.CM).type( AUTOMATIC )),
                        p(bold(text("Перечень workflow:"))).alignCentered().indentFirstLine( 0.25, RtfUnit.CM ),
                        p(text(strWorkflow)),
                        p( picture( getResource( "WorkflowProcess2.png" ) ).size(15, 10, RtfUnit.CM).type( AUTOMATIC ))
                        );
                   
                
                for  (String s:saWorkflow) {
                   myRtf.section((row(s).leftCellBorder().rightCellBorder().topCellBorder().bottomCellBorder().cellSpace(5, CM)));
                }
                                
            }           
        }
        
            
        //Data Map Import
        Article DataMapImport= articleDao.findByTitle("DataMapImport");
        String sDataMapImport=DataMapImport.getArticle();       
        if (sDataMapImport.isEmpty())
        {
        } else {
            myRtf
                    .section(p(sDataMapImport));
        }
        

        
        //Replace SRF
        Article SRFReplacing= articleDao.findByTitle("SRFReplacing");
        String sSRFReplacing=SRFReplacing.getArticle();   
        if (sSRFReplacing.isEmpty())
        {
        } else {
            myRtf
                    .section(p(sSRFReplacing));
        }       
        
        myRtf
                .section(
              RtfSectionFormatAndHeaderFooter.headerForAllPages(p( subscript("Date: "), subscript(currentDate()), pageBreak(),subscript("Владимир Вашурин т.+380675772841" )))
        )
        .section(
              RtfSectionFormatAndHeaderFooter.footerOnAllPages(p( bold("ООО \"Ареон Консалтинг\" "), currentPageNumber()  ).alignRight()))
        ;
        
        myRtf.out(  i);

    i.close();
    
    //RTF document open
        try
    {
      Desktop.getDesktop().open( out );
    }
    catch ( IOException e ) {}
  }
}



public static InputStream getResource(String resource) throws Exception {
   ClassLoader cl = Thread.currentThread().getContextClassLoader();
   InputStream is = cl.getResourceAsStream(resource);
   return is;
}
 
}

