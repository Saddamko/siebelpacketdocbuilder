/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation;

import myshop.biz.ua.SiebelCRMDocumentation.dao.ArticleDao;
import myshop.biz.ua.SiebelCRMDocumentation.service.impl.ArticleDaoImpl;

/**
 *
 * @author vashurinvlad
 */
public class Factory {
    public static Factory instance = new Factory ();
    public static ArticleDao articleDao;
    private Factory (){}
    public static Factory getInstance(){
        return Factory.instance;
    }
    
    public static ArticleDao getArticleDao(){
        if (articleDao == null) articleDao = new ArticleDaoImpl();
        return articleDao;
    }
}
