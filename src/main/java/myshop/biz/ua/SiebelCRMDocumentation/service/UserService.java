/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation.service;

/**
 *
 * @author VashurinVlad
 */
import java.util.List;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.User;

public interface UserService {

    User addUser(User user);
    void delete(long id);
    //User getByName(String name);
    User editUser(User user);
    //List<User> getAll();

    public void saveUser(User user);
}

