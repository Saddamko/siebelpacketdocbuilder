/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.User;
import myshop.biz.ua.SiebelCRMDocumentation.Repository.UserRepository;
import myshop.biz.ua.SiebelCRMDocumentation.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        User savedUser = userRepository.saveAndFlush(user);

        return savedUser;
    }

    public void delete(User id) {
        userRepository.delete(id);
    }

 /*   @Override
    public User getByName(String name) {
        return userRepository.findByName(name);
    }
*/

    @Override
    public User editUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //userRepository.delete(id);
    }

    @Override
    public void saveUser(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}

