/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myshop.biz.ua.SiebelCRMDocumentation.service.impl;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Query;
import myshop.biz.ua.SiebelCRMDocumentation.Entity.Article;
import myshop.biz.ua.SiebelCRMDocumentation.HibernateUtil;
import myshop.biz.ua.SiebelCRMDocumentation.dao.ArticleDao;
import org.hibernate.Session;

/**
 *
 * @author vashurinvlad
 */
public class ArticleDaoImpl implements ArticleDao {

    @Override
    public void addArticle(Article article) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(article);
            session.getTransaction().commit();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if ((session != null)&& (session.isOpen())) session.close();
        }
        session.close();
    }

    @Override
    public void deleteArticle(Article article) throws SQLException {
                Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(article);
            session.getTransaction().commit();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if ((session != null)&& (session.isOpen())) session.close();
        }
        session.close();
    }

    @Override
    public Article getArticle(int id) throws SQLException {
        Article result = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            result = (Article) session.load(Article.class, id);       
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if ((session != null)&& (session.isOpen())) session.close();
        }
        session.close();
        return result;
    }

    @Override
    public List<Article> getArticles() throws SQLException {
        List<Article> articles = null;
                Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            articles = session.createCriteria(Article.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } //finally {if ((session != null)&& (session.isOpen())) session.close();}
        session.close();
        return articles;
    }
    
    @Override
    public Article findById(int id) {
    return HibernateUtil.getSessionFactory().openSession().get(Article.class, id);
    }
    
    @Override
    public Article findByTitle(String Title) {
    Session session = null;
    session = HibernateUtil.getSessionFactory().openSession();
    Query query = session.createQuery("from Article where title = :title");
    //query.setParameter("title", "%" + Title + "%");
    query.setParameter("title", Title);
    Article article = null;
    article = (Article) query.getSingleResult();
    return article;        
    }
    
    
}
